// 1. Retrieve data for ids : [2, 13, 23].
// 2. Group data based on companies.
//         { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
// 3. Get all data for company Powerpuff Brigade
// 4. Remove entry with id 2.
// 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
// 6. Swap position of companies with id 93 and id 92.
// 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.


const fs = require("fs");
const path = require("path");

function solution() {
    const pathData = path.join(__dirname, "data.json");
    
    fs.readFile(pathData, { encoding: "utf-8", flag: "r"}, (err, data) => {
        if (err) {
            console.error(`Error when reading ${pathData}`);
            console.error(err);

            return;
        } else {
            console.log(`Successfully read ${pathData}`);

            data = JSON.parse(data);
            
            // Solution 1
            
            const ids = [2, 13, 23];
            const pathQuestion1 = path.join(__dirname, "output1.json");

            const question1Data = data.employees.filter((employee) => {
                return ids.indexOf(employee.id) > -1;
            });

            fs.writeFile(pathQuestion1, JSON.stringify(question1Data), (err) => {
                if (err) {
                    console.error(`Error when reading ${pathQuestion1}`);
                    console.error(err);
                    
                    return;
                } else {
                    console.log(`Successfully written data to ${pathQuestion1}`);

                    // Solution 2

                    const pathQuestion2 = path.join(__dirname, "output2.json");

                    const question2Data = data.employees.reduce((groupedCompanies, employee) => {
                        const companyName = employee.company;

                        if (groupedCompanies.hasOwnProperty(companyName) === false) {
                            groupedCompanies[companyName] = [];
                        }

                        groupedCompanies[companyName].push(employee);

                        return groupedCompanies;
                    }, {});

                    fs.writeFile(pathQuestion2, JSON.stringify(question2Data), (err) => {
                        if (err) {
                            console.error(`Error when writing data to ${pathQuestion2}`, err);
                            console.error(err);
                            
                            return;
                        } else{
                            console.log(`Successfully written to ${pathQuestion2}`);

                            // Solution 3

                            const requiredCompanyName = "Powerpuff Brigade";
                            const pathQuestion3 = path.join(__dirname, "output3.json");

                            const question3Data = data.employees.filter((employee) => {

                                return requiredCompanyName === employee.company;
                            });

                            fs.writeFile(pathQuestion3, JSON.stringify(question3Data), (err) => {
                                if (err) {
                                    console.error(`Error when writing data to ${pathQuestion3}`, err);
                                    console.error(err);
                            
                                    return;
                                } else {
                                    console.log(`Successfully written to ${pathQuestion3}`);

                                    // Solution 4

                                    const pathQuestion4 = path.join(__dirname, "output4.json");

                                    const question4Data = data.employees.filter((employee) => {
                                        return employee.id !== 2;
                                    });

                                    fs.writeFile(pathQuestion4, JSON.stringify(question4Data), (err) => {
                                        if (err) {
                                            console.error(`Error when writing data to ${pathQuestion4}`, err);
                                            console.error(err);
                            
                                            return;
                                        } else {
                                            console.log(`Successfully written data to ${pathQuestion4}`);

                                            // Solution 5
                                            const pathQuestion5 = path.join(__dirname, "output5.json");

                                            const question5Data = data.employees.sort((a, b) => {
                                                let companyA = a.company.toLowerCase();
                                                let companyB = b.company.toLowerCase();

                                                // converted to same case and removing whitespace and hyphen characters to compare only alphabets
                                                companyA = companyA.replaceAll(" ","")
                                                        .replaceAll("-", "");
                                                
                                                companyB = companyB.replaceAll(" ", "")
                                                        .replaceAll("-", "");
                                                
                                                if ( companyA < companyB) {
                                                    return -1;
                                                } else if ( companyA > companyB) {
                                                    return 1;
                                                } else {
                                                    return a.id - b.id;
                                                }
                                            });

                                            fs.writeFile(pathQuestion5, JSON.stringify(question5Data), (err) => {
                                                if (err) {
                                                    console.error(`Error when writing data to ${pathQuestion5}`, err);
                                                    console.error(err);
                            
                                                    return;
                                                } else {
                                                    console.log(`Successfully written data to ${pathQuestion5}`);

                                                    // Solution 6

                                                    const pathQuestion6 = path.join(__dirname, "output6.json");
                                                    // making a deep copy of original data
                                                    const question6Data = JSON.parse(JSON.stringify(data));

                                                    const indexesToSwap = data.employees.reduce((indexesToSwap, employee, index) => {
                                                        if ( employee.id === 93 || employee.id === 92) {
                                                            indexesToSwap.push(index);
                                                        }

                                                        return indexesToSwap;
                                                    }, []);

                                                    // swapping positions in the array
                                                    [question6Data.employees[indexesToSwap[0]], question6Data.employees[indexesToSwap[1]]] = 
                                                        [question6Data.employees[indexesToSwap[1]], question6Data.employees[indexesToSwap[0]]];

                                                    // console.log( data, question6Data);

                                                    fs.writeFile(pathQuestion6, JSON.stringify(question6Data), (err) => {
                                                        if (err) {
                                                            console.error(`Error when writing data to ${pathQuestion5}`, err);
                                                            console.error(err);

                                                            return;
                                                        } else {
                                                            console.log(`Successfully written data to ${pathQuestion6}`);

                                                            // Solution 7
                                                            const pathQuestion7 = path.join(__dirname, "output7.json");
                                                            const question7Data = data.employees.reduce((question7Data, employee) => {
                                                                if ( employee.id % 2 === 0) {
                                                                    const date = new Date();
                                                                    employee["birthday"] = `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
                                                                }

                                                                question7Data.employees.push(employee);

                                                                return question7Data;
                                                            }, {employees: []});

                                                            fs.writeFile(pathQuestion7, JSON.stringify(question7Data), (err) => {
                                                                if (err) {
                                                                    console.error(`Error when writing data to ${pathQuestion7}`, err);
                                                                    console.error(err);

                                                                    return;
                                                                } else {
                                                                    console.log(`Successfully written data ${pathQuestion7}`);
                                                                }
                                                            });
                                                        }
                                                    });
                                                    
                                                }
                                            });
                                        }
                                    });

                                }
                            });
                        }
                    });
                    // console.log(question2Data);
                }
            });

        }
    });
}

console.log(solution());